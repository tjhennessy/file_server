if [ -d "build" ]; then
    rm -rf build/
fi
cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Debug
cd build
make -j 8
