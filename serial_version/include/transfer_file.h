/**
 * @file transfer_file.h
 */
#ifndef _TRANSFER_FILE_H
#define _TRANSFER_FILE_H
#include <stdint.h>
/**
 * @brief Takes a file stored as an array of uint8_t bytes, prefixes it with
 *        a length then sends it to client_sock_fd.
 * @param client_sock_fd A client TCP socket connection
 * @param out_buffer Array of uint8_t bytes containing file data
 * @param num_bytes Number of bytes contained in out_buffer
 * @return 0 on success, -1 on failure
 */
int transfer_file(int client_sock_fd, uint8_t *out_buffer, size_t num_bytes);
#endif /* _TRANSFER_FILE_H */