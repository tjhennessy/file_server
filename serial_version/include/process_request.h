/**
 * @file process_request.h
 */
#ifndef _PROCESS_REQUEST_H
#define _PROCESS_REQUEST_H
/**
 * @brief Retrieves path and file name from client.
 * 
 * 1. Sends prompt to user of the form:
 * 
 * Bytes  0        1                    length
 *        +--------+--------------------+
 * Desc.  | length |        value       |
 *        +--------+--------------------+
 * 
 * 2. Receives request from user of the form:
 * 
 * Bytes  0      1                2                    length + 2
 *        +------+----------------+--------------------+
 * Desc.  | type | length of path | path and file name |
 *        +------+----------------+--------------------+
 * 
 * NOTE: field two in request header, length of path, should always
 *       include the total size of the string plus one for the NULL byte
 * 
 * types: download file - 0, upload file - 1
 * 
 * @param client_sock_fd Socket client connection
 * @return Byte array corresponding to size of filename, or NULL
 *         on failure.
 * @note Assumes socket is an established client connection.
 * @note The returned byte array must be freed by caller.
 */
uint8_t *process_request(int client_sock_fd);
#endif /* _PROCESS_REQUEST_H */