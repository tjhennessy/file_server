/**
 * @file accept_connections.h
 */
#ifndef _ACCEPT_CONNECTIONS_H
#define _ACCEPT_CONNECTIONS_H
/**
 * @brief Allows clients to connect to file server.
 * @param server_sock_fd File descriptor for server's
 *        listening socket.
 * @return Postive value corresponding to connected client
 *         socket on success, or -1 on failure.
 * @note Assumes server_sock_fd is an established AF_INET,
 *       SOCK_STREAM
 */
int accept_connections(int server_sock_fd);
#endif /* _ACCEPT_CONNECTIONS_H */