#ifndef _READ_FILE_H
#define _READ_FILE_H
#include <stdint.h>
/**
 * @brief Returns file as a null terminated array of bytes.
 * @param path String containing path and file name
 * @return A null terminated array of bytes containig the file
 */
uint8_t *read_file(uint8_t *path);
#endif /* _READ_FILE_H */