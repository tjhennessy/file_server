/**
 * @file get_tcp_server.h
 */
#ifndef _GET_TCP_SERVER_H
#define _GET_TCP_SERVER_H
/**
 * @brief Binds any available local address to port returning a socket
 *        listening for client connections.
 * 
 * @param port String representation of port number
 * @returns Non-negative socket file descriptor on success, or
 *          -1 on failure.
 * @note Assumes a valid port is passed in
 */
int get_tcp_server(const char *port);
#endif /* _GET_TCP_SERVER_H */