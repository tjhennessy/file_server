#ifndef _SEND_ALL_H
#define _SEND_ALL_H
/**
 * @brief Sends buf_len bytes from out_buf to sock_fd.
 * @param sock_fd Socket file descriptor connected to client.
 * @param out_buf Array of bytes ready for transmitting.
 * @param buf_len Number of bytes in out_buf.
 * @return 0 on success, or -1 if error occurs while attempting to
 *         send.
 */
int send_all(int sock_fd, uint8_t *out_buf, size_t buf_len);
#endif /* _SEND_ALL_H */