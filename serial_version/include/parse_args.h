/**
 * @file parse_args.h
 */
#ifndef _PARSE_ARGS_H
#define _PARSE_ARGS_H

#define POW(BASE, POWER) ((BASE) << (POWER))
#define WIRE_PROT_PATH_SZ 8 /* Currently uses 1 byte to store path length */
#define MAX_PATH_LEN POW(1, WIRE_PROT_PATH_SZ)

typedef struct arg_ctx
{
    struct sockaddr_in addr;
    char path[MAX_PATH_LEN + 1];
    size_t path_len;
    uint8_t request_type;
} arg_ctx_t;

/**
 * @brief Processes arg_count strings from command line
 *        returning an arg_ctx_t structure.
 * 
 * Example usage for requesting file from server:
 *   ftc -i <IPv4 address> -p <port> -g <file>
 * 
 *   -i If not present, localhost is used
 *   -p If not present, random port > 1024 used
 * 
 * @param arg_count Number of command line arguments
 * @param arg_strings Command line arguments
 * @return An arg_ctx_t structure or NULL on failure
 * @note Caller must free memory for arg_ctx_t
 * @note Does not check params for bad values nor does it do any
 *       validation
 * @note Returns sockaddr_in values in network byte order and 
 *       only handles IPv4
 */
arg_ctx_t *parse_args(int arg_count, char **arg_strings);

#endif /* _PARSE_ARGS_H */