#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <accept_connections.h>

int accept_connections(int server_sock_fd)
{
    struct sockaddr_in client_addr;
    socklen_t clnt_addr_len = sizeof(client_addr);
    int client_sock_fd = accept(server_sock_fd, 
                               (struct sockaddr *) &client_addr, 
                               &clnt_addr_len);
    if (client_sock_fd < 0)
    {
        fprintf(stderr, "Error: call to accept() did not work\n");
        return -1;
    }
    
    #ifdef DEBUG
        char clnt_info[INET_ADDRSTRLEN];
        const char *ret = inet_ntop(AF_INET, 
                                    &(client_addr.sin_addr.s_addr), 
                                    clnt_info, 
                                    sizeof(clnt_info));
        if (ret != NULL)
        {
            printf("connection from %s:%d\n", clnt_info, 
                                            ntohs(client_addr.sin_port));
        }
    #endif

    return client_sock_fd;
}