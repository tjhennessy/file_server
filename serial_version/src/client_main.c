/**
 * @file client.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <parse_args.h>
#include <send_all.h>
#include <recv_all.h>

/*
    Large, Medium, and Small size files for testing
    "/usr/share/dict/words";
    // "/home/hennessy/Code/Networking/file_server/serial_version/files/plato_crito.txt";
    // "/home/hennessy/Code/Networking/file_server/serial_version/files/file.txt";
*/

enum request_type
{
    download = 0,
    upload   = 1
};

enum protocol_sizes
{
    request_type_size = 1, // Request type will be less than 256
    path_length_size  = 1, // Request path string will always be less than 256
    header_size       = 2  // Space for above two fields
};

/* ---------------- START OF CLEANUP CALLBACKS ----------------*/

/**
 * @see parse_args.c for definition of arg_ctx_t
 */
static void free_arg_ctx(arg_ctx_t **arg)
{
    #if DEBUG
        printf("freeing arg_ctx_t struct in free_arg_ctx\n");
    #endif

    if (*arg != NULL)
    {
        free(*arg);
    }
}

static void close_conn(int *conn)
{
    #if DEBUG
        printf("closing connection in close_connection\n");
    #endif

    if (*conn != -1)
    {
        close(*conn);
    }
}

static void free_memory32(uint32_t **buffer)
{
    if (*buffer != NULL)
    {
        free(*buffer);
    }
}

static void free_memory8(uint8_t **buffer)
{
    if (*buffer != NULL)
    {
        free(*buffer);
    }
}

static void close_file(FILE **fp)
{
    if (*fp != NULL)
    {
        fclose(*fp);
    }
}

/* ---------------- END OF CLEANUP CALLBACKS ----------------*/

static int make_request(int sock_fd, arg_ctx_t *request_info)
{
    // Assumes sock_fd valid and request_info valid
    // The only binary data is not multi-byte therefore no endianness concerns
    uint8_t request_buffer[header_size + request_info->path_len];
    uint8_t path_length;
    int rv;

    #if DEBUG
        printf("request type: %hhu, ", request_info->request_type);
        printf("path length: %zu, ", request_info->path_len);
        printf("path: %s\n", request_info->path);
    #endif 

    // The first two fields are currently one byte, if they grow to multi-byte
    // then they will need to account for endianness
    memcpy(request_buffer, &request_info->request_type, request_type_size);
    // request_info->path_len is type size_t store into uint8_t just to be safe
    path_length = request_info->path_len;
    memcpy(request_buffer + request_type_size, 
           &path_length, 
           path_length_size);
    memcpy(request_buffer + request_type_size + path_length_size, 
           request_info->path, 
           request_info->path_len);

    rv = send_all(sock_fd, 
                  request_buffer, 
                  request_info->path_len + header_size);
    if (rv < 0)
    {
        fprintf(stderr, "call to send_all() failed\n");
        return -1;
    }

    #if DEBUG
        printf("client request sent successfully\n");
    #endif

    return 0;
}

// Caller must free returned uint8_t * memory
static uint8_t *download_file(int sock_fd, uint32_t *num_bytes)
{
    uint32_t *header __attribute__ ((__cleanup__(free_memory32))) = NULL;
    uint8_t *file_buffer;

    header = (uint32_t *) recv_all(sock_fd, sizeof(uint32_t));
    if (NULL == header)
    {
        fprintf(stderr, "call to recv_all() failed\n");
        return NULL;
    }
    memcpy(num_bytes, header, sizeof(uint32_t));
    *num_bytes = ntohl(*num_bytes);

    #if DEBUG
        printf("client preparing to receive %d bytes\n", *num_bytes);
    #endif

    file_buffer = recv_all(sock_fd, *num_bytes);
    if (NULL == file_buffer)
    {
        fprintf(stderr, "call to recv_all() failed\n");
        return NULL;
    }

    #if DEBUG
        printf("file transfer complete\n");
    #endif

    return file_buffer;
}

// TODO: Upload file function

int main(int argc, char *argv[])
{
    arg_ctx_t *req_info __attribute__ ((__cleanup__(free_arg_ctx))) = NULL;
    int sock_fd __attribute__ ((__cleanup__(close_conn))) = -1;
    uint8_t *file_buffer __attribute__ ((__cleanup__(free_memory8))) = NULL;
    FILE *ofp __attribute__ ((__cleanup__(close_file))) = NULL;
    uint32_t num_bytes;
    int rv;

    req_info = parse_args(argc, argv);
    if (NULL == req_info)
    {
        fprintf(stderr, "call to parse_args() failed\n");
        return 1;
    }

    sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock_fd < 0)
    {
        perror("call to socket() failed");
        return 1;
    }

    rv = connect(sock_fd, 
                (struct sockaddr *) &(req_info->addr), 
                sizeof(struct sockaddr_in));
    if (rv < 0)
    {
        perror("call to connect() failed");
        return 1;
    }

    #if DEBUG
        printf("client connected to server\n");
    #endif

    rv = make_request(sock_fd, req_info);
    if (rv < 0)
    {
        fprintf(stderr, "call to make_request() failed\n");
        return 1;
    }

    file_buffer = download_file(sock_fd, &num_bytes);
    if (NULL == file_buffer)
    {
        fprintf(stderr, "call to download_file() failed\n");
        return 1;
    }

    #if DEBUG
        printf("file downloaded from server\n");
    #endif

    // TODO: Get name from path and create new path string of the form
    //       tmp/<name>
    ofp = fopen("test.txt", "w");
    if (NULL == ofp)
    {
        perror("call to fopen() failed");
        return 1;
    }

    rv = fwrite((void *) file_buffer, sizeof(uint8_t), (size_t) num_bytes, ofp);
    if (rv != (int) num_bytes)
    {
        fprintf(stderr, "call to fwrite() failed\n");
        return 1;
    }

    #if DEBUG
        printf("file transfer complete\n");
    #endif
    
    return 0;
}