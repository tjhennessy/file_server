/**
 * @file parse_args.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <parse_args.h>

static void print_usage(char *program)
{
    fprintf(stderr, "Usage: %s -i <IPv4 address> -p <port> -g <file>\n", program);
}

arg_ctx_t *parse_args(int arg_count, char **arg_strings)
{
    int opt;
    int rv;
    arg_ctx_t *req_info; // Must be freed by caller

    if (arg_count != 7)
    {
        print_usage(arg_strings[0]);
        return NULL;
    }

    req_info = (arg_ctx_t *) malloc(sizeof(arg_ctx_t));
    if (NULL == req_info)
    {
        fprintf(stderr, "call to malloc() failed in parse_args\n");
        return NULL;
    }

    memset(req_info, 0, sizeof(arg_ctx_t));

    req_info->addr.sin_family = AF_INET; /* Using IPv4 */

    while ((opt = getopt(arg_count, arg_strings, "i:p:g:")) != -1)
    {
        switch (opt)
        {
            case 'i': /* IPv4 address */
                rv = inet_pton(AF_INET, optarg, &(req_info->addr.sin_addr));
                if (rv != 1)
                {
                    fprintf(stderr, "call to inet_pton() failed.\n");
                    free(req_info);
                    return NULL;
                }
            break;
            case 'p': /* port */
                req_info->addr.sin_port = htons(atoi(optarg));
            break;
            case 'g': /* file to get */
                req_info->request_type = 0; // download request
                req_info->path_len = strlen(optarg) + 1;
                memcpy(req_info->path, optarg, req_info->path_len);
            break;
            // add case here in future for upload
            default: /* ? */
                print_usage(arg_strings[0]);
                free(req_info);
                return NULL;
        }
    }

    return req_info;
}