#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <get_tcp_server.h>
#include <accept_connections.h>
#include <process_request.h>
#include <read_file.h>
#include <transfer_file.h>

static void close_conn(int *fd)
{
    #if DEBUG
        printf("closing fd = %d\n", *fd);
    #endif
    close(*fd);
}

static void free_mem(uint8_t **buffer)
{
    #if DEBUG
        printf("calling free on buffer\n");
    #endif
    if (buffer != NULL)
    {
        free(*buffer);
    }
}

// TODO: Signal Handler
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <port number>\n", argv[0]);
        return 1;
    }

    int server_sock_fd __attribute__ ((__cleanup__(close_conn)));
    server_sock_fd = get_tcp_server(argv[1]);
    if (server_sock_fd < 0)
    {
        fprintf(stderr, "Server not established.\n");
        return 1;
    }

    int client_sock_fd __attribute__ ((__cleanup__(close_conn)));
    client_sock_fd = accept_connections(server_sock_fd);
    if (client_sock_fd < 0)
    {
        fprintf(stderr, "Client could not connect.\n");
        return 1;
    }

    uint8_t *request_buffer __attribute__ ((__cleanup__(free_mem))) = NULL;
    request_buffer = process_request(client_sock_fd);
    if (NULL == request_buffer)
    {
        fprintf(stderr, "Could not process client's request.\n");
        return 1;
    }
    
    #if DEBUG
        printf("request_buffer contains: %s\n", request_buffer);
    #endif 
    
    uint8_t *file_buffer __attribute__ ((__cleanup__(free_mem))) = NULL;
    file_buffer = read_file(request_buffer);
    if (NULL == file_buffer)
    {
        fprintf(stderr, "Call to read_file() failed\n");
        return 1;
    }

    #if DEBUG
        printf("size of file is %zu\n", strlen((char *) file_buffer) + 1);
        printf("file contains: %s\n", file_buffer);
    #endif

    int rv;
    int file_size = strlen((char *) file_buffer) + 1;
    rv = transfer_file(client_sock_fd, 
                       file_buffer, 
                       strlen((char *) file_buffer) + 1);
    if (rv != 0)
    {
        fprintf(stderr, "Call to transfer_file() failed.\n");
        return 1;
    }
    
    return 0;
}