/**
 * @file pwrtirocess_request.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdint.h>
#include <unistd.h>
#include <process_request.h>
#include <send_all.h>
#include <recv_all.h>


/**
 * @note These sizes are very important for the wire protocol.
 *       Other files rely on these sizes for handling the protocol
 *       appropriately.
 */
typedef enum sizes
{   // sizes in bytes
    req_type_sz = 1,
    path_len_sz = 1,
    prmp_len_sz = 1
} field_sizes;

typedef enum type
{
    download_req   = 0,
    upload_req  = 1
} request_type;

/*
// Not currently used
static void release_memory(uint8_t **buffer)
{
    #if DEBUG
        printf("freeing memory using release_memory.\n");
    #endif
    free(*buffer);
}

static uint8_t *make_prompt(char *prompt, size_t length)
{
    // prefix length to prompt and return
    uint8_t *buffer = malloc(sizeof(uint8_t) * (length + prmp_len_sz));
    if (NULL == buffer)
    {
        fprintf(stderr, "call to malloc() failed.\n");
        return NULL;
    }

    // prefix length of prompt
    memcpy(buffer, (void *) &length, prmp_len_sz);
    if (NULL == buffer)
    {
        fprintf(stderr, "call to memcpy() failed.\n");
        free(buffer);
        return NULL;
    }

    // append prompt
    memcpy(buffer + prmp_len_sz, (void *) prompt, length);
    if (NULL == (buffer + prmp_len_sz))
    {
        fprintf(stderr, "call to memcpy() failed.\n");
        free(buffer);
        return NULL;
    }

    return buffer;    
}


static int prompt_client(int client_sock_fd)
{
    int rv;
    char *prompt = "File Server version 1.0\nPlease enter filename: ";
    size_t prompt_length = strlen(prompt) + 1;
    uint8_t *prompt_buf __attribute__((__cleanup__(release_memory))); 
    prompt_buf = make_prompt(prompt, prompt_length);
    if (NULL == prompt_buf)
    {
        fprintf(stderr, "call to make_prompt() failed.\n");
        return -1;
    }

    rv = send_all(client_sock_fd, prompt_buf, prompt_length + prmp_len_sz);
    if (rv < 0)
    {
        fprintf(stderr, "call to send_all() failed.\n");
        return -1;
    }

    return 0;
}
*/

static void free_mem(uint8_t **buffer)
{
    #if DEBUG
        printf("freeing uint8_t buffer in free_mem\n");
    #endif

    if (buffer != NULL)
    {
        free(*buffer);
    }
}

// Need to free returned byte array
static uint8_t *get_client_response(int client_sock_fd)
{
    uint8_t *req_type __attribute__ ((__cleanup__(free_mem)));
    uint8_t *path_len __attribute__ ((__cleanup__(free_mem)));
    uint8_t *path;
    
    // read request type
    req_type = recv_all(client_sock_fd, req_type_sz);
    if (NULL == req_type)
    {
        fprintf(stderr, "call to recv_all() failed.\n");
        return NULL;
    }
    #if DEBUG
        printf("type is %hhu\n", *req_type);
    #endif

    path_len = recv_all(client_sock_fd, path_len_sz);
    if (NULL == path_len)
    {
        fprintf(stderr, "call to recv_all() failed.\n");
        return NULL;
    }
    #if DEBUG
        printf("message length is %hhu\n", *path_len);
    #endif

    path = recv_all(client_sock_fd, *path_len);
    if (NULL == path)
    {
        fprintf(stderr, "call to recv_all() failed.\n");
        return NULL;
    }
    #if DEBUG
        printf("path string is %s\n", path);
    #endif

    return path;
}

uint8_t *process_request(int client_sock_fd)
{
    uint8_t *client_response;
    
    client_response = get_client_response(client_sock_fd);
    if (NULL == client_response)
    {
        fprintf(stderr, "call to get_client_response() failed.\n");
        return NULL;
    }

    #if DEBUG
        printf("client response received\n");
    #endif 

    return client_response;
}