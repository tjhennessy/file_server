/**
 * @file transfer_file.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>
#include <send_all.h>

// prefix length header
// return bytes array with header + payload
// need to free memory somewhere else
static uint8_t *pack(uint8_t *out_buffer, uint32_t num_bytes)
{
    uint32_t length;
    uint8_t *bytes;

    length = htonl(num_bytes);

    #if DEBUG
        printf("transfering num_bytes = %d\n", num_bytes);
    #endif

    bytes = (uint8_t *) calloc(num_bytes + sizeof(uint32_t), sizeof(uint8_t));
    if (NULL == bytes)
    {
        fprintf(stderr, "Call to malloc() failed in pack()\n");
        return NULL;
    }

    memcpy(bytes, &length, sizeof(uint32_t));
    memcpy(bytes + sizeof(uint32_t), out_buffer, num_bytes);

    return bytes;    
}

static void free_mem(uint8_t **buffer)
{
    #if DEBUG
        printf("freeing memory in free_mem()\n");
    #endif
    if (buffer != NULL)
    {
        free(*buffer);
    }
}

// take a buffer of uint8_t bytes, its size
// send its size htons(uint16_t)
// sendall the actual bytes next
int transfer_file(int client_sock_fd, uint8_t *out_buffer, size_t num_bytes)
{
    uint8_t *packet __attribute__ ((__cleanup__(free_mem))) = NULL;
    int rv;

    packet = pack(out_buffer, num_bytes);
    if (NULL == packet)
    {
        fprintf(stderr, "Call to pack() failed in transfer_file()\n");
        return -1;
    }

    #if DEBUG
        printf("%s\n", packet);
    #endif

    rv = send_all(client_sock_fd, packet, num_bytes + sizeof(uint32_t));
    if (rv != 0)
    {
        fprintf(stderr, "Call to send_all() failed in transfer_file()\n");
        return -1;
    }
    
    return 0;
}