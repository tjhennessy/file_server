#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <send_all.h>

int send_all(int sock_fd, uint8_t *out_buf, size_t buf_len)
{
    int bytes_just_sent;
    int total_bytes_sent = 0;
    int bytes_remaining = buf_len;
    
    #if DEBUG
        printf("send_all() to socket %d\n", sock_fd);
    #endif

    if (NULL == out_buf)
    {
        fprintf(stderr, "send_all() passed by argument.\n");
        return -1;
    }

    while (total_bytes_sent < (int) buf_len)
    {
        bytes_just_sent = write(sock_fd, 
                                out_buf + total_bytes_sent, 
                                bytes_remaining);
        if (bytes_just_sent < 0)
        {
            perror("call to write() failed");
            return -1;
        }
        total_bytes_sent += bytes_just_sent;
        bytes_remaining -= bytes_just_sent;
        bytes_just_sent = 0;
    }

    return 0;
}