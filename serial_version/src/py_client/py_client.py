#!/usr/bin/env python3

import sys
import socket
import parser
import request_info
import server

class py_client:
    def __init__(self, cmd_args):
        self.info = self.get_command_args(cmd_args)
        self.conn = self.establish_connection(self.info.server_ip, self.info.server_port)
        self.file = self.get_file()
        print(self.file)
        # write file wit file_writer
        self.teardown()

    def get_command_args(self, args):
        return request_info.request_info(parser.parser(args).get_args())

    def establish_connection(self, server_ip, server_port):
        return server.server(server_ip, server_port)
    
    def teardown(self):
        self.conn.disconnect()

    def get_file(self):
        self.conn.connect_to_server()
        infile = self.conn.download_file(self.info.file_path)
        return infile

    def write_file(self, path, file):
        pass

if __name__ == '__main__':
    py_client(sys.argv)