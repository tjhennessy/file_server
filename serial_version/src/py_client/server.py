#!/usr/bin/env python3

import socket
import struct

class server:

    Request_Types = {'read' : 0, 'write' : 1}
    Protocol_Sizes = {'request_size' : 1, 
                      'path_len_size' : 1, 
                      'file_size_bytes': 4}

    def __init__(self, ip='localhost', port=1991):
        self.server_ip = ip
        self.server_port = port
        self.sock = self.connect_to_server()

    def connect_to_server(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.server_ip, self.server_port))
        return sock
    
    def disconnect(self):
        # self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def download_file(self, path):
        self.make_request(path)
        size = self.recvall(self.Protocol_Sizes['file_size_bytes'])
        size = struct.unpack('i', size)
        size = socket.ntohl(size[0])
        file = self.recvall(size)
        return file
    
    def make_request(self, path):
        request_type = self.Request_Types['read']
        path_length = len(path)
        # Sent as bytes, but no multi-byte data -> not worried about endianness
        request = struct.pack('BB', request_type, path_length)
        request = request + path.encode()
        bytes_sent = self.sendall(request, path_length + 2)
        if bytes_sent != (path_length + 2):
            raise Exception("call to send_all() failed")

    def sendall(self, packet, num_bytes):
        total_bytes_sent = 0
        while total_bytes_sent < num_bytes:
            sent_bytes = self.sock.send(packet)
            if 0 == sent_bytes:
                raise RuntimeError("socket connection closed")
            total_bytes_sent = total_bytes_sent + sent_bytes
        return total_bytes_sent
    
    def recvall(self, num_bytes):
        all_data = b''
        while len(all_data) < num_bytes:
            data = self.sock.recv(num_bytes - len(all_data))
            if not data:
                raise RuntimeError('socket connection closed')
            all_data += data
        return all_data
