class request_info:
    def __init__(self, info=None):
        if not info:
            raise Exception('info passed empty to request_info')
        
        self.server_ip = info[0]
        self.server_port = info[1]
        self.file_path = info[2]
    
    def __str__(self):
        return self.server_ip + ' ' \
        + str(self.server_port) + ' ' \
        + self.file_path