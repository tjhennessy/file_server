#!/usr/bin/env python3

import sys
import argparse 

class parser:
    def __init__(self, cmd_args):
        self.program_name = cmd_args[0]
        self.args = cmd_args[1:] # remove first arugment program name
        self.parser = argparse.ArgumentParser(prog=self.program_name, 
                                              usage="%(prog)s [options]")
        self.parser.add_argument('-i', '--ip', 
                                 help='server ipv4 address', 
                                 required='True', 
                                 default='localhost')
        self.parser.add_argument('-p', '--port',
                                 help='port of file server',
                                 type=int,
                                 required='True',
                                 default='1991')
        self.parser.add_argument('-g', '--getfile',
                                 help='path and name of file',
                                 required='True',
                                 default='/usr/share/dict/words')

    def get_args(self):
        results = self.parser.parse_args(self.args)
        return (results.ip, results.port, results.getfile)
                    

if __name__ == '__main__':
    print(parser(sys.argv).get_args())