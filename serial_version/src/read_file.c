/**
 * @file read_file.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <read_file.h>

static void close_file(FILE **f)
{
    #if DEBUG
        printf("closing file in close_file.\n");
    #endif
    if (f != NULL)
    {
        fclose(*f);
    }
}

uint8_t *read_file(uint8_t *path)
{
    FILE *ifp __attribute__ ((__cleanup__(close_file))) = NULL;
    uint8_t *file_buffer;
    int file_size;
    int bytes_read;

   ifp = fopen((const char *) path, "r");
   if (NULL == ifp)
   {
       perror("call to fopen() failed");
       return NULL;
   }

   fseek(ifp, 0L, SEEK_END);
   file_size = ftell(ifp);
   rewind(ifp);

    #if DEBUG
        printf("file size on disk: %d\n", file_size);
    #endif 

    file_buffer = (uint8_t *) calloc(file_size + 1, sizeof(uint8_t));
    if (NULL == file_buffer)
    {
        fprintf(stderr, "call to malloc() failed in read_file().\n");
        return NULL;
    }

    bytes_read = fread(file_buffer, sizeof(uint8_t), file_size, ifp);
    if (bytes_read != file_size)
    {
        fprintf(stderr, "call to fread() failed.\n");
        free(file_buffer);
        return NULL;
    }

    return file_buffer;
}