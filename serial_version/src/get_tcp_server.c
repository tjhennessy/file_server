/**
 * @file get_tcp_server.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define MAX_BACKLOG 4

static void free_addrinfo(struct addrinfo **ai)
{
    #if DEBUG
        printf("releasing memory for addrinfo\n.");
    #endif

    if (ai != NULL)
    {
        freeaddrinfo(*ai);
    }
}

int get_tcp_server(const char *port)
{
    int rv;
    int server_sock_fd;
    int optval;
    struct addrinfo hints;
    struct addrinfo *servinfo __attribute__ ((__cleanup__(free_addrinfo))) = NULL;
    struct addrinfo *p;

    memset(&hints, 0, sizeof(hints)); // must be done to avoid errors
    hints.ai_family = AF_UNSPEC;      // IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;  // TCP
    hints.ai_flags = AI_PASSIVE;      // use local IP

    rv = getaddrinfo(NULL, port, &hints, &servinfo);
    if (rv != 0)
    {
        fprintf(stderr, "getaddrinfo %s\n", gai_strerror(rv));
        return -1;
    }

    // Attempt to get socket for each result in linked list
    // Attempt to bind each successful socket until successful
    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        server_sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (server_sock_fd < 0)
        {
            continue;
        }

        rv = bind(server_sock_fd, p->ai_addr, p->ai_addrlen);
        if (rv < 0)
        {
            close(server_sock_fd);
            continue;
        }

        break; // received a socket and bound to address
    }

    // There exists a chance where socket was not bound leaving loop above
    if (NULL == p)
    {
        fprintf(stderr, "server could not bind\n");
        return -1;
    }

    optval = 1;
    setsockopt(server_sock_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    rv = listen(server_sock_fd, MAX_BACKLOG);
    if (rv < 0)
    {
        fprintf(stderr, "call to listen() did not work\n");
        close(server_sock_fd);
        return -1;
    }

    #ifdef DEBUG
        printf("server listening on file descriptor %d.\n", server_sock_fd);
    #endif

    return server_sock_fd;
}