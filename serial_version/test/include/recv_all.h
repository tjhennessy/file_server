/**
 * @file recv_all.h
 */
#ifndef _RECV_ALL_H
#define _RECV_ALL_H
/**
 * @brief Reads num_bytes_to_read bytes from file descriptor sock_fd
 *        and returns contents in form of uint8_t array.
 * @param sock_fd Socket data is recevied from
 * @param num_bytes_to_read Positive integer corresponding to expected
 *        number of bytes to be read from sock_fd.
 * @return On success a dynamically allocated uint8_t array of size 
 *         num_bytes_to_read, on failure NULL.
 * @note On a successful call, the memory allocated for the uint8_t
 *       array needs to be freed by the caller.
 */
uint8_t *recv_all(int sock_fd, size_t num_bytes_to_read);
#endif /* _RECV_ALL_H */