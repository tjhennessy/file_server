#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <send_all.h>
#include <recv_all.h>

static void close_sock(int *sock_fd)
{
    #if DEBUG
        printf("closing sock %d\n", *sock_fd);
    #endif

    if (*sock_fd != -1)
    {
        close(*sock_fd);
    }
}

static void free_sockaddr(struct sockaddr_in **addr)
{
    #if DEBUG
        printf("freeing sockaddr_in\n");
    #endif
    if (*addr != NULL)
    {
        free(*addr);
    }
}

static void free_uint8_mem(uint8_t **buffer)
{
    #if DEBUG
        printf("freeing uint8_t array\n");
    #endif
    if (buffer != NULL)
    {
        free(*buffer);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        fprintf(stderr, "usage: %s <address> <port> <path>\n", argv[0]);
        return 1;
    }

    int sock_fd __attribute__ ((__cleanup__(close_sock))) = -1;
    struct sockaddr_in *addr __attribute__ ((__cleanup__(free_sockaddr))) = NULL;
    int rv;
    socklen_t addr_len;
    
    sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock_fd < 0)
    {
        perror("call to socket() failed");
        return 1;
    }

    addr = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
    if (NULL == addr)
    {
        fprintf(stderr, "call to malloc() failed.\n");
        return 1;
    }

    addr_len = sizeof(struct sockaddr_in);
    addr->sin_family = AF_INET;
    addr->sin_port = htons(atoi(argv[2]));
    rv = inet_pton(AF_INET, argv[1], &addr->sin_addr);
    if (rv != 1)
    {
        fprintf(stderr, "call to inet_pton() failed.\n");
        return 1;
    }

    rv = connect(sock_fd, (struct sockaddr *) addr, addr_len);
    if (rv < 0)
    {
        perror("call to connect() failed");
        return 1;
    }

    uint8_t request_type = 0;
    char *path_string = "/usr/share/dict/words";
    // "/home/hennessy/Code/Networking/file_server/serial_version/files/plato_crito.txt";
    // "/home/hennessy/Code/Networking/file_server/serial_version/files/file.txt";
    uint8_t path_length = strlen(path_string) + 1;
    uint8_t req_buf[path_length + 2]; // 2 is for header
    memset(req_buf, 0, path_length + 2); 
    memcpy(req_buf, &request_type, 1);
    memcpy(req_buf + 1, &path_length, 1);
    memcpy(req_buf + 2, path_string, path_length);
    rv = send_all(sock_fd, req_buf, path_length + 2);

    
    uint8_t *header __attribute__ ((__cleanup__(free_uint8_mem))) = NULL;
    header = recv_all(sock_fd, sizeof(uint32_t));

    uint32_t size;
    memcpy(&size, header, sizeof(uint32_t));
    size = htonl(size);

    #if DEBUG
        printf("client preparing to receive file of size: %u\n", size);
    #endif

    uint8_t *file_buf __attribute__ ((__cleanup__(free_uint8_mem))) = NULL;
    file_buf = recv_all(sock_fd, size);
    if (NULL == file_buf)
    {
        fprintf(stderr, "call to recv_all failed, could not get file_buffer\n");
        return 1;
    }

    // open file and write buffer to it

    printf("file contains:\n%s\n", file_buf);
    
    return 0;
}