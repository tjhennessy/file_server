#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <recv_all.h>
#include <send_all.h>

uint8_t *recv_all(int sock_fd, size_t num_bytes_to_read)
{
    int bytes_just_read;
    int remaining_bytes = num_bytes_to_read;
    int total_bytes_read = 0;
    uint8_t *in_buf;

    in_buf = (uint8_t *) malloc(num_bytes_to_read);
    if (NULL == in_buf)
    {
        fprintf(stderr, "call to malloc failed.\n");
        return NULL;
    }
    memset(in_buf, 0, num_bytes_to_read);

    #if DEBUG
        printf("attempting to receive %zu bytes in recv_all()\n", 
                num_bytes_to_read);
    #endif
    
    while (total_bytes_read < num_bytes_to_read)
    {
        bytes_just_read = read(sock_fd, 
                               in_buf + total_bytes_read, 
                               remaining_bytes);
        if (bytes_just_read < 0)
        {
            perror("call to read() failed");
            free(in_buf);
            return NULL;
        }
        total_bytes_read += bytes_just_read;
        remaining_bytes -= bytes_just_read;
        bytes_just_read = 0;
    }

    return in_buf;
}